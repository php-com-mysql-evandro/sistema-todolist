<?php

require_once $_SERVER['DOCUMENT_ROOT'] .'/helpers/Config.php';
require_once $_SERVER['DOCUMENT_ROOT'] .'/models/Usuario.php';

function validarLogin(){
    $login = null;
    if(!empty($_POST)){
        $login['email'] = $_POST['email'];
        $login['senha'] = $_POST['senha'];
        $login['manterLogado'] = $_POST['manterLogado'] ?? 0;

        $usuario = consultarDadoUsuario($login['email']);
        if ($usuario){

            if (password_verify($login['senha'],$usuario['senha'])){
                header('Location:/admin');
            }else{
                $_SESSION['mensagem']='Usuário ou Senha inválido';
                return $login;
            }

        }else{
            $_SESSION['mensagem']='Usuário ou Senha inválido';
            return $login;
        }
    }

}

function visualizar($id){

    $plano = buscarPlano($id);

    return $plano;

}

function cadastrar(){
    $plano = [];
    if(!empty($_POST)){
        $plano = [
            'titulo' => $_POST['titulo'],
            'valor' => $_POST['valor'],
            'descricao' => $_POST['descricao']
        ];

        if (cadastrarPlano($plano)) {
            header("Location:/admin/plano");
            exit;
        }
    }
    return $plano;
}

function editar($id){
    $plano = [];
    if(!empty($_POST)){
        $plano = [
            'titulo' => $_POST['titulo'],
            'valor' => $_POST['valor'],
            'descricao' => $_POST['descricao'],
        ];

        
        // $plano['titulo'] = $_POST['titulo'];
        // $plano['valor'] = $_POST['valor'];
        // $plano['descricao'] = $_POST['descricao'];
        

        if (editarPlano($plano, $id)) {
            header("Location:/admin/plano");
            exit;
        }
    }
    return buscarPlano($id);
     

}

function deletar($id){
    if(deletarPlano($id)){
        header("Location:/admin/plano");
        exit;
    }
    deletarPlano($id);
}
?>