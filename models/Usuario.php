<?php 
require_once $_SERVER['DOCUMENT_ROOT'] .'/helpers/Config.php';
require_once BANCO_DE_DADOS;

function consultarDadoUsuario($busca){
    $db = conexao();
    $sql = "SELECT * FROM usuarios WHERE email=:email OR token=:token";

    try{
        $stmt = $db->prepare($sql);
        $stmt->bindParam(':email',$busca,PDO::PARAM_STR);
        $stmt->bindParam(':token',$busca,PDO::PARAM_STR);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);

    }catch(PDOException $e){
        die($e->getMessage());
        return false;
    }
}

function salvarTokenUsuario($token,$id){
    $db = conexao();
    $sql = "UPDATE usuarios SET token=:token WHERE id=:id ";
    try{
        $stmt = $db->prepare($sql);
        $stmt->bindParam(":token",$token,PDO::PARAM_INT);
        $stmt->bindParam(":id",$id,PDO::PARAM_INT);
        $stmt->execute();
        return true;
    }catch(PDOException $e){
        die($e->getMessage());
        return false;
    }
}
?>